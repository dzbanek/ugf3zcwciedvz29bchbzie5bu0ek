package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"url-collector/internal"
	"url-collector/internal/controller"
	"url-collector/internal/nasa"
	"url-collector/internal/router"
	"url-collector/internal/service"
)

func main() {
	config := internal.NewConfigFromEnv()
	imageClient := nasa.NewImageClient(config.NasaApiBaseURL, config.NasaApiKey)
	imageService := service.NewImageService(config, imageClient)
	imageController := &controller.ImageController{
		Service: imageService,
	}

	if !config.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	serviceRouter := router.NewRouter(imageController)
	httpServer := &http.Server{
		Addr:    fmt.Sprintf(":%v", config.Port),
		Handler: serviceRouter,
	}

	go func() {
		err := httpServer.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Println(err)
			panic(err)
		}
	}()

	stopSignal := make(chan os.Signal)
	signal.Notify(stopSignal, syscall.SIGINT, syscall.SIGTERM)
	<-stopSignal

	shutdownContext, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	err := httpServer.Shutdown(shutdownContext)
	if err != nil {
		panic(err)
	}

	imageService.Stop()
}
