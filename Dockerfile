FROM golang:1.19 AS builder
WORKDIR /home/url-collector
COPY . /home/url-collector
ENV CGO_ENABLED=false
RUN make test && CGO_ENABLED=0 make build

FROM alpine:latest
WORKDIR /home/url-collector
ENV PORT=8080
ENV API_KEY=DEMO_KEY
ENV CONCURRENT_REQUESTS=5
ENV DEBUG=false
COPY --from=builder /home/url-collector/bin/url-collector /home/url-collector/url-collector
CMD ["./url-collector"]