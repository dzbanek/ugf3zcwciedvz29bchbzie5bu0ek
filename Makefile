mockery:
	go install github.com/vektra/mockery/v2@latest

mocks: mockery
	rm -rf ./internal/mocks
	mockery  --dir internal --all --keeptree --output internal/mocks

test:
	go test -race ./... -coverprofile=coverage.out
	go tool cover -func=coverage.out
	rm -f coverage.out

build:
	go build -o bin/url-collector ./cmd/main.go

run:
	go run cmd/main.go || true