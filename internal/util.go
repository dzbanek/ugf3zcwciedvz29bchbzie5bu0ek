package internal

import (
	"math"
	"time"
)

// DaysBetween calculates the amount of days passed between
// the two dates. If the result has a decimal point (ex. 5.5 days)
// it returns rounded value (6). If not it returns incremented value
// to supply a closed time range.
func DaysBetween(from time.Time, to time.Time) int {
	days := to.Sub(from).Hours() / 24
	if math.Mod(days, 1) != 0 {
		return int(math.Ceil(days))
	}

	return int(days + 1)
}

// NextDay just adds 24 hours to given time struct
func NextDay(date time.Time) time.Time {
	return date.Add(24 * time.Hour)
}
