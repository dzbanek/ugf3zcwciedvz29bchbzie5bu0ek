package internal

import (
	"context"
)

type Provider struct {
	Client         Client
	RequestChannel chan *FindImageTask
	closeChannel   chan any
}

// NewProvider returns a new Provider instance with provided client, request channel
// and a close channel
func NewProvider(client Client, requestChannel chan *FindImageTask) *Provider {
	return &Provider{
		Client:         client,
		RequestChannel: requestChannel,
		closeChannel:   make(chan any, 1),
	}
}

// FindImageTask contains all the data needed for the provider
// and the given client to get the image from the external API
// and the data to return result (error and result channels)
type FindImageTask struct {
	Context       context.Context
	Opts          *FindImageOpts
	ResultChannel chan *Image
	ErrorChannel  chan error
}

// Run method of Provider is mentioned to listen on shared request channel
// to grab a task to run and return the result on a dedicated channel (given
// by the service). Each instance of provider is one goroutine handling the
// requests asynchronously. Based on how many Provider instances are run the
// given amount of parallel requests are processed. This is a simple form of
// asynchronous processing and rate limiting.
func (p *Provider) Run() {
	running := true
	for running {
		select {
		case <-p.closeChannel:
			running = false
			break

		case task := <-p.RequestChannel:
			image, err := p.Client.FindImage(task.Context, task.Opts)
			if err != nil {
				task.ErrorChannel <- err
				continue
			}

			task.ResultChannel <- image
		}
	}
}

// Stop submits an empty struct on a close channel to break the for loop in Run's goroutine
func (p *Provider) Stop() {
	p.closeChannel <- struct{}{} // Request stop
}
