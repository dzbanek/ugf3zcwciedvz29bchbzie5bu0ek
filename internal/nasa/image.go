package nasa

// Image entity of the NASA's API response
type Image struct {
	Copyright      string `json:"copyright"`
	Date           string `json:"date"`
	Explanation    string `json:"explanation"`
	HDImageURL     string `json:"hdurl"`
	ImageURL       string `json:"url"`
	MediaType      string `json:"media_type"`
	ServiceVersion string `json:"service_version"`
	Title          string `json:"title"`
}
