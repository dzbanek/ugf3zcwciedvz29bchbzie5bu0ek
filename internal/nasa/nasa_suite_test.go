package nasa_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestNasa(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Nasa Suite")
}
