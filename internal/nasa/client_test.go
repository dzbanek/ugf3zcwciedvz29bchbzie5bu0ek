package nasa_test

import (
	"context"
	"encoding/json"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"net/http"
	"net/http/httptest"
	"time"
	"url-collector/internal"
	"url-collector/internal/nasa"
)

var _ = Describe("FindImage", func() {
	var (
		client         internal.Client
		sampleResponse *nasa.Image
		sampleError    *nasa.APIError
		apiKey         string
		ctx            context.Context
	)

	BeforeEach(func() {
		apiKey = uuid.New().String()
		ctx = context.Background()
		sampleError = &nasa.APIError{
			Message: "error",
			Code:    500,
		}

		sampleResponse = &nasa.Image{
			Copyright:      "test",
			Date:           "2022-01-01",
			Explanation:    "test",
			HDImageURL:     "http://onet.pl",
			ImageURL:       "http://onet.pl",
			MediaType:      "png",
			ServiceVersion: "v1",
			Title:          "test",
		}
	})

	When("got image successfully", func() {
		It("should return a proper response", func() {
			var receivedRequest *http.Request
			serverURL := httptest.NewServer(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
				receivedRequest = request
				response, err := json.Marshal(sampleResponse)
				Expect(err).NotTo(HaveOccurred())
				writer.Header().Set("Content-Type", "application/json")
				writer.WriteHeader(http.StatusOK)

				_, err = writer.Write(response)
				Expect(err).NotTo(HaveOccurred())
			})).URL

			client = nasa.NewImageClient(serverURL, apiKey)
			now := time.Now()
			result, err := client.FindImage(ctx, &internal.FindImageOpts{
				Date: now,
			})

			Expect(err).NotTo(HaveOccurred(), "no error occurred")
			Expect(result).NotTo(BeNil(), "got result")
			Expect(result.URL.String()).To(Equal(sampleResponse.ImageURL), "got valid URL")
			Expect(receivedRequest.URL.Query().Get("api_key")).To(Equal(apiKey), "valid api key passed")
			Expect(receivedRequest.URL.Query().Get("date")).To(Equal(now.Format("2006-01-02")), "got valid date")
		})
	})

	When("got API error", func() {
		It("should fail with an error", func() {
			var receivedRequest *http.Request
			serverURL := httptest.NewServer(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
				receivedRequest = request
				response, err := json.Marshal(sampleError)
				Expect(err).NotTo(HaveOccurred())
				writer.Header().Set("Content-Type", "application/json")
				writer.WriteHeader(http.StatusInternalServerError)

				_, err = writer.Write(response)
				Expect(err).NotTo(HaveOccurred())
			})).URL

			client = nasa.NewImageClient(serverURL, apiKey)
			now := time.Now()
			result, err := client.FindImage(ctx, &internal.FindImageOpts{
				Date: now,
			})

			Expect(err).To(HaveOccurred(), "an error occurred")
			Expect(result).To(BeNil(), "got no result")
			Expect(receivedRequest.URL.Query().Get("api_key")).To(Equal(apiKey), "valid api key passed")
			Expect(receivedRequest.URL.Query().Get("date")).To(Equal(now.Format("2006-01-02")), "got valid date")
		})
	})

	When("got no response", func() {
		It("should fail with an error", func() {
			client = nasa.NewImageClient("http://localhost", apiKey)
			now := time.Now()
			result, err := client.FindImage(ctx, &internal.FindImageOpts{
				Date: now,
			})

			Expect(err).To(HaveOccurred(), "an error occurred")
			Expect(result).To(BeNil(), "got no result")
		})
	})
})
