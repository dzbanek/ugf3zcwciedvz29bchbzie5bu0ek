package nasa

// APIError returned from the NASA's API
type APIError struct {
	Message string `json:"msg"`
	Code    int    `json:"code"`
}
