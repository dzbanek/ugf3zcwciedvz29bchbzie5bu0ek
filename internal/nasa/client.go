package nasa

import (
	"context"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/google/go-querystring/query"
	"net/http"
	"net/url"
	"url-collector/internal"
)

const (
	apiKeyQueryField = "api_key"
	imageEndpoint    = "/planetary/apod"
	timeFormat       = "2006-01-02"
)

// imageClient is the implementation of Client interface
// using NASA's API
type imageClient struct {
	httpClient *resty.Client
}

// NewImageClient accepts the baseURL (for the NASA API) and the API key
// and initializes the go-resty client with this URL and the API key set
// by default in the appropriate query field
func NewImageClient(baseURL string, apiKey string) internal.Client {
	httpClient := resty.New()

	httpClient.SetQueryParams(map[string]string{
		apiKeyQueryField: apiKey,
	})

	httpClient.SetBaseURL(baseURL)

	return &imageClient{
		httpClient: httpClient,
	}
}

// FindImageQuery contains all the query parameters to be
// included in get image request. These values are automatically
// set as the query fields thanks to "go-querystring" lib.
type FindImageQuery struct {
	Date string `url:"date"`
}

func (i *imageClient) FindImage(ctx context.Context, opts *internal.FindImageOpts) (*internal.Image, error) {
	queryData := &FindImageQuery{
		Date: opts.Date.Format(timeFormat),
	}

	queryParams, err := query.Values(queryData)
	if err != nil {
		return nil, fmt.Errorf("failed to build query: %s", err)
	}

	imageData := &Image{}
	apiError := &APIError{}
	response, err := i.httpClient.R().
		SetContext(ctx).
		SetQueryParamsFromValues(queryParams).
		SetResult(imageData).
		SetError(apiError).
		Get(imageEndpoint)

	if err != nil {
		return nil, fmt.Errorf("failed to get image: %s", err)
	}

	statusCode := response.StatusCode()
	if statusCode != http.StatusOK {
		return nil, fmt.Errorf("got invalid %v response: %s", statusCode, apiError.Message)
	}

	imageURL, err := url.Parse(imageData.ImageURL)
	if err != nil {
		return nil, fmt.Errorf("failed to parse image URL: %s", err)
	}

	return &internal.Image{
		URL: imageURL,
	}, nil
}
