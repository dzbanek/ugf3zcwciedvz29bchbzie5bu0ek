package internal

import (
	"context"
	"net/url"
	"time"
)

type FindImageOpts struct {
	Date time.Time
}

type Image struct {
	URL *url.URL
}

// Client is a generic interface to implement in order to handle
// any API providing image URLs. This interface is used by service
// to get the image URL by given params. In order to use a different
// datasource it's enough to supply a new Client implementation
type Client interface {
	FindImage(ctx context.Context, opts *FindImageOpts) (*Image, error)
}
