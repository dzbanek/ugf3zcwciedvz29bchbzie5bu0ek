package controller

import (
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
	"url-collector/internal/service"
)

type ImageController struct {
	Service service.ImageService
}

var (
	errInvalidTimeRange = errors.New("invalid time range")
)

type GetImagesQuery struct {
	From time.Time `form:"from" binding:"required" time_format:"2006-01-02"`
	To   time.Time `form:"to" binding:"required" time_format:"2006-01-02"`
}

type GetImagesResponse struct {
	Urls []string `json:"urls"`
}

func validateGetImagesRequest(query *GetImagesQuery) error {
	if query.To.Before(query.From) {
		return errInvalidTimeRange
	}

	return nil
}

func (i *ImageController) GetImages(c *gin.Context) {
	query := &GetImagesQuery{}
	err := c.BindQuery(query)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err = validateGetImagesRequest(query)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	images, err := i.Service.GetImages(c, &service.GetImagesOpts{
		From: query.From,
		To:   query.To,
	})

	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	var urls []string
	for _, image := range images {
		urls = append(urls, image.URL.String())
	}

	c.JSON(http.StatusOK, &GetImagesResponse{
		Urls: urls,
	})
}
