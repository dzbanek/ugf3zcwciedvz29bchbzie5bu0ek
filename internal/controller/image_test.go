package controller_test

import (
	"errors"
	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/mock"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"url-collector/internal"
	"url-collector/internal/controller"
	mocks "url-collector/internal/mocks/service"
	"url-collector/internal/router"
)

var _ = Describe("GetImages", func() {
	var (
		ginContext *gin.Context
		service    *mocks.ImageService
		ctrl       *controller.ImageController
		httpWriter *httptest.ResponseRecorder
	)

	BeforeEach(func() {
		gin.SetMode(gin.TestMode)
		service = new(mocks.ImageService)
		ctrl = &controller.ImageController{
			Service: service,
		}

		httpWriter = httptest.NewRecorder()
		ginRouter := router.NewRouter(ctrl)
		ginContext = gin.CreateTestContextOnly(httpWriter, ginRouter)
		ginContext.Request = &http.Request{
			Header: make(http.Header),
		}
	})

	When("got empty request", func() {
		It("should fail with an error", func() {
			requestURL, err := url.Parse("http://localhost/pictures")
			Expect(err).NotTo(HaveOccurred())

			ginContext.Request.URL = requestURL
			ctrl.GetImages(ginContext)

			response := httpWriter.Result()
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest), "got bad request response")
		})
	})

	When("to date missing", func() {
		It("should fail with an error", func() {
			requestURL, err := url.Parse("http://localhost/pictures?from=2022-01-01")
			Expect(err).NotTo(HaveOccurred())

			ginContext.Request.URL = requestURL
			ctrl.GetImages(ginContext)

			response := httpWriter.Result()
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest), "got bad request response")
		})
	})

	When("from date missing", func() {
		It("should fail with an error", func() {
			requestURL, err := url.Parse("http://localhost/pictures?to=2022-01-01")
			Expect(err).NotTo(HaveOccurred())

			ginContext.Request.URL = requestURL
			ctrl.GetImages(ginContext)

			response := httpWriter.Result()
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest), "got bad request response")
		})
	})

	When("from after to", func() {
		It("should fail with an error", func() {
			requestURL, err := url.Parse("http://localhost/pictures?to=2022-01-01&from=2022-01-02")
			Expect(err).NotTo(HaveOccurred())

			ginContext.Request.URL = requestURL
			ctrl.GetImages(ginContext)

			response := httpWriter.Result()
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest), "got bad request response")
		})
	})

	When("service layer failed", func() {
		It("should fail with an error", func() {
			requestURL, err := url.Parse("http://localhost/pictures?from=2021-12-30&to=2022-01-01")
			Expect(err).NotTo(HaveOccurred())

			sampleError := errors.New("sample error")
			service.On("GetImages", mock.Anything, mock.Anything).
				Return(nil, sampleError).
				Once()

			ginContext.Request.URL = requestURL
			ctrl.GetImages(ginContext)

			response := httpWriter.Result()
			Expect(response.StatusCode).To(Equal(http.StatusInternalServerError), "got internal error")
		})
	})

	When("service layer succeeded", func() {
		It("should return a proper response", func() {
			requestURL, err := url.Parse("http://localhost/pictures?from=2021-12-30&to=2022-01-01")
			Expect(err).NotTo(HaveOccurred())

			imageURL, err := url.Parse("http://yourImage.com")
			Expect(err).NotTo(HaveOccurred())
			images := []*internal.Image{
				{
					URL: imageURL,
				},
			}

			service.On("GetImages", mock.Anything, mock.Anything).
				Return(images, nil).
				Once()

			ginContext.Request.URL = requestURL
			ctrl.GetImages(ginContext)

			response := httpWriter.Result()
			Expect(response.StatusCode).To(Equal(http.StatusOK), "got OK response")

			responseBody, err := io.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			Expect(string(responseBody)).To(ContainSubstring(imageURL.String()), "got image URL")
		})
	})
})
