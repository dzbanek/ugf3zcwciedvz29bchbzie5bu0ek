package internal

import (
	"github.com/spf13/viper"
)

type Config struct {
	MaxConcurrentRequests int
	NasaApiBaseURL        string
	NasaApiKey            string
	Port                  int
	TaskTimeoutSeconds    int
	Debug                 bool
}

func NewConfigFromEnv() *Config {
	viper.AutomaticEnv()
	viper.SetDefault("CONCURRENT_REQUESTS", 5)
	viper.SetDefault("NASA_API_BASE_URL", "https://api.nasa.gov")
	viper.SetDefault("API_KEY", "DEMO_KEY")
	viper.SetDefault("PORT", 8080)
	viper.SetDefault("TASK_TIMEOUT_SECONDS", 30)

	return &Config{
		MaxConcurrentRequests: viper.GetInt("CONCURRENT_REQUESTS"),
		NasaApiBaseURL:        viper.GetString("NASA_API_BASE_URL"),
		NasaApiKey:            viper.GetString("API_KEY"),
		Port:                  viper.GetInt("PORT"),
		TaskTimeoutSeconds:    viper.GetInt("TASK_TIMEOUT_SECONDS"),
		Debug:                 viper.GetBool("DEBUG"),
	}
}
