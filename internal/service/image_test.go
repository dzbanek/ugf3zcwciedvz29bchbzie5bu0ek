package service_test

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/mock"
	"net/url"
	"time"
	"url-collector/internal"
	"url-collector/internal/mocks"
	"url-collector/internal/service"
)

var _ = Describe("GetImages", func() {

	var (
		mockedImageClient *mocks.Client
		imageService      service.ImageService
		config            *internal.Config
		ctx               context.Context
		sampleError       error
	)

	BeforeEach(func() {
		config = &internal.Config{
			NasaApiKey:            uuid.New().String(),
			MaxConcurrentRequests: 5,
			TaskTimeoutSeconds:    1,
		}

		mockedImageClient = new(mocks.Client)
		imageService = service.NewImageService(config, mockedImageClient)
		ctx = context.Background()
		sampleError = errors.New("sample error")
	})

	Context("successful flow", func() {
		When("got image successfully", func() {
			It("should return a valid result", func() {
				image := &internal.Image{
					URL: &url.URL{
						Host:   "google.com",
						Scheme: "http",
						Path:   "/your-image",
					},
				}

				mockedImageClient.On("FindImage", mock.Anything, mock.Anything).
					Return(image, nil).
					Once()

				result, err := imageService.GetImages(ctx, &service.GetImagesOpts{
					From: time.Now(),
					To:   time.Now(),
				})

				Expect(err).NotTo(HaveOccurred(), "no error occurred")
				Expect(result).To(HaveLen(1), "got one image")
				Expect(result[0]).To(Equal(image), "got underlying image entity")
			})
		})

		When("got multiple images successfully", func() {
			It("should return all the images", func() {
				const daysCount = 5
				var images []*internal.Image

				now := time.Now()
				start := now.Add(-daysCount * 24 * time.Hour)
				day := start
				for i := 0; i <= daysCount; i++ {
					image := &internal.Image{
						URL: &url.URL{
							Host:   "google.com",
							Scheme: "http",
							Path:   fmt.Sprintf("image-no-%v", i),
						},
					}

					images = append(images, image)
					opts := &internal.FindImageOpts{Date: day}
					mockedImageClient.On("FindImage", mock.Anything, opts).
						Return(image, nil).
						Once()

					day = internal.NextDay(day)
				}

				result, err := imageService.GetImages(ctx, &service.GetImagesOpts{
					From: start,
					To:   now,
				})

				Expect(err).NotTo(HaveOccurred(), "no error occurred")
				Expect(result).To(HaveLen(len(images)), "got all images")

				for _, image := range images {
					Expect(result).To(ContainElement(image), "got valid image array")
				}
			})
		})
	})

	When("failed to get image", func() {
		It("should return an error", func() {
			mockedImageClient.On("FindImage", mock.Anything, mock.Anything).
				Return(nil, sampleError).
				Once()

			result, err := imageService.GetImages(ctx, &service.GetImagesOpts{
				From: time.Now(),
				To:   time.Now(),
			})

			Expect(err).To(HaveOccurred(), "an error occurred")
			Expect(result).To(BeNil(), "got no images")
			Expect(err).To(Equal(sampleError), "underlying error returned")
		})
	})

	When("failed to get at least one image", func() {
		It("should return an error", func() {
			const daysCount = 5

			now := time.Now()
			start := now.Add(-daysCount * 24 * time.Hour)

			mockedImageClient.On("FindImage", mock.Anything, mock.Anything).
				Return(nil, sampleError).
				Once()

			mockedImageClient.On("FindImage", mock.Anything, mock.Anything).
				Return(&internal.Image{}, nil).
				Times(daysCount)

			result, err := imageService.GetImages(ctx, &service.GetImagesOpts{
				From: start,
				To:   now,
			})

			Expect(err).To(HaveOccurred(), "an error occurred")
			Expect(result).To(BeNil(), "got no images")
			Expect(err).To(Equal(sampleError), "underlying error returned")
		})
	})

	When("request timed out", func() {
		It("should return an error", func() {
			mockedImageClient.On("FindImage", mock.Anything, mock.Anything).
				Return(&internal.Image{}, nil).
				After(time.Duration(config.TaskTimeoutSeconds+1) * time.Second).
				Once()

			result, err := imageService.GetImages(ctx, &service.GetImagesOpts{
				From: time.Now(),
				To:   time.Now(),
			})

			Expect(err).To(HaveOccurred(), "an error occurred")
			Expect(result).To(BeNil(), "got no images")
			Expect(err.Error()).To(ContainSubstring("timeout"), "timeout error returned")
		})
	})

	When("request dropped", func() {
		It("should return no result", func() {
			mockedImageClient.On("FindImage", mock.Anything, mock.Anything).
				Return(&internal.Image{}, nil).
				After(time.Duration(config.TaskTimeoutSeconds) * time.Second).
				Once()

			ctx, cancel := context.WithCancel(ctx)
			time.AfterFunc(time.Duration(config.TaskTimeoutSeconds/3)*time.Second, func() {
				cancel()
			})

			result, err := imageService.GetImages(ctx, &service.GetImagesOpts{
				From: time.Now(),
				To:   time.Now(),
			})

			Expect(err).To(HaveOccurred(), "got an error")
			Expect(err.Error()).To(ContainSubstring("canceled"), "got context cancelled error")
			Expect(result).To(BeNil(), "got no images")
		})
	})

})
