package service

import (
	"context"
	"errors"
	"time"
	"url-collector/internal"
)

type ImageService interface {
	GetImages(context.Context, *GetImagesOpts) ([]*internal.Image, error)
	Stop()
}

type imageService struct {
	imageTaskChannel chan *internal.FindImageTask
	config           *internal.Config
	providers        []*internal.Provider
}

var (
	errTimeout = errors.New("timeout waiting for API responses")
)

// NewImageService initializes the instance of inner service struct (with encapsulated fields)
// and initializes the requested amount of providers (parallel processors).
func NewImageService(config *internal.Config, client internal.Client) ImageService {
	var providers []*internal.Provider
	taskChannel := make(chan *internal.FindImageTask, 1)
	for i := 0; i < config.MaxConcurrentRequests; i++ {
		provider := internal.NewProvider(client, taskChannel)
		providers = append(providers, provider)
		go provider.Run()
	}

	return &imageService{
		imageTaskChannel: taskChannel,
		config:           config,
		providers:        providers,
	}
}

type GetImagesOpts struct {
	From time.Time
	To   time.Time
}

func (s *imageService) GetImages(ctx context.Context, opts *GetImagesOpts) ([]*internal.Image, error) {
	rangeSize := internal.DaysBetween(opts.From, opts.To)
	resultChannel := make(chan *internal.Image, rangeSize)
	errorChannel := make(chan error, rangeSize)

	day := opts.From
	for i := 0; i < rangeSize; i++ {
		task := &internal.FindImageTask{
			Context:       ctx,
			ResultChannel: resultChannel,
			ErrorChannel:  errorChannel,
			Opts: &internal.FindImageOpts{
				Date: day,
			},
		}

		s.imageTaskChannel <- task
		day = internal.NextDay(day)
	}

	taskTimeoutCtx, cancel := context.WithTimeout(ctx, time.Duration(s.config.TaskTimeoutSeconds)*time.Second)
	defer cancel()

	var images []*internal.Image
	for i := 0; i < rangeSize; i++ {
		select {
		case image := <-resultChannel:
			images = append(images, image)
		case err := <-errorChannel:
			return nil, err
		case <-ctx.Done():
			return nil, ctx.Err()
		case <-taskTimeoutCtx.Done():
			return nil, errTimeout
		}
	}

	return images, nil
}

// Stop is called on server shutdown. Its purpose is to close all the providers.
func (s *imageService) Stop() {
	for _, provider := range s.providers {
		provider.Stop()
	}
}
