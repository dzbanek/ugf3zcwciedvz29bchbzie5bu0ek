package router

import (
	"github.com/gin-gonic/gin"
	"url-collector/internal/controller"
)

// ErrorHandler is mentioned to provide a nice error message in the
// REST response when any error occurred. If any error occurred during
// the request processing the first error value will be set in the "error"
// field of JSON response.
func ErrorHandler(c *gin.Context) {
	c.Next()
	if len(c.Errors) > 0 {
		c.JSON(c.Writer.Status(), gin.H{"error": c.Errors[0].Error()})
	}
}

func NewRouter(controller *controller.ImageController) *gin.Engine {
	router := gin.Default()
	router.Use(ErrorHandler)
	router.GET("/pictures", controller.GetImages)
	return router
}
