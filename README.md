# url-collector

An interview task done by Paweł Danek.

## Description

This is a microservice responsible for retrieving the image URLs from external image providers. For now it integrates only with NASA image service, however it can be easily extended to support various providers.

## Get image

For now this service supports only one request - `GET /pictures`. This endpoint accepts date range as a query parameter and returns an array of URLs in JSON response.

### Query parameters

| Name |   Format   |   Description    |         Validation         |
|:----:|:----------:|:----------------:|:--------------------------:|
| from | YYYY-MM-DD | Range start date |  Can't be after end date   |
|  to  | YYYY-MM-DD |  Range end date  | Can't be before start date |

### Sample response

```json
{"urls":["https://apod.nasa.gov/apod/image/2201/lunararcs_caxete_960.jpg","https://apod.nasa.gov/apod/image/2201/LeonardTail_Hattenbach_960.jpg"]}%
```

### Example cURL request
 ```text
 ❯ curl -vvv 'localhost:8080/pictures?from=2022-01-02&to=2022-01-03'
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /pictures?from=2022-01-02&to=2022-01-03 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.85.0
> Accept: */*
> 
[GIN] 2023/01/05 - 18:25:00 | 200 |  1.561514458s |       127.0.0.1 | GET      "/pictures?from=2022-01-02&to=2022-01-03"
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=utf-8
< Date: Thu, 05 Jan 2023 17:25:00 GMT
< Content-Length: 146
< 
* Connection #0 to host localhost left intact
{"urls":["https://apod.nasa.gov/apod/image/2201/lunararcs_caxete_960.jpg","https://apod.nasa.gov/apod/image/2201/LeonardTail_Hattenbach_960.jpg"]}%   
 ```

### Sample error

When any error occurs application returns appropriate HTTP response (with appropriate status code) and an error body (JSON struct). You can find your error in "error" field.

For instance:
 ```json
{"error":"invalid time range"}
 ```

## Testing

Application is covered with unit tests written with BDD framework (Ginkgo). To run all the test suits and see some coverage info just use `test` make suite.

For instance:
```text
❯ make test
go test -race ./... -coverprofile=coverage.out
?       url-collector/cmd       [no test files]
?       url-collector/internal  [no test files]
ok      url-collector/internal/controller       0.797s  coverage: 100.0% of statements
?       url-collector/internal/mocks    [no test files]
?       url-collector/internal/mocks/service    [no test files]
ok      url-collector/internal/nasa     0.444s  coverage: 90.0% of statements
?       url-collector/internal/router   [no test files]
ok      url-collector/internal/service  1.630s  coverage: 100.0% of statements
go tool cover -func=coverage.out
url-collector/internal/controller/image.go:28:  validateGetImagesRequest        100.0%
url-collector/internal/controller/image.go:36:  GetImages                       100.0%
url-collector/internal/nasa/client.go:28:       NewImageClient                  100.0%
url-collector/internal/nasa/client.go:49:       FindImage                       87.5%
url-collector/internal/service/image.go:25:     NewImageService                 100.0%
url-collector/internal/service/image.go:47:     GetImages                       100.0%
total:                                          (statements)                    96.7%
rm -f coverage.out
```

## Running

Application can be both run as a local binary (built on the target machine) and as a docker container.

### Local run

Use `make run` command. This will start the server on your local machine.

### Docker run

Dockerfile is provided within the repository. Use `docker build -t url-collector .` to build the image and `docker run -ti url-collector` to run the image.

## Run arguments

Server supports multiple run arguments passed as environment variables.

|         Name         |  Type   |       Default        |                                        Description                                        | 
|:--------------------:|:-------:|:--------------------:|:-----------------------------------------------------------------------------------------:|
|         PORT         | integer |         8080         |                               Port for server to listen on                                |
|        DEBUG         |  bool   |        false         |                      Whether gin should run in release or debug mode                      |
|       API_KEY        | string  |       DEMO_KEY       |                        API key for the NASA API to use in requests                        |
| CONCURRENT_REQUESTS  | integer |          5           | How many concurrent streams should be used to<br/>obtain the images from the external API |
|  NASA_API_BASE_URL   | string  | https://api.nasa.gov |                             The base URL for NASA's image API                             |
| TASK_TIMEOUT_SECONDS |   int   |          30          |             Amount of seconds to trigger a timeout<br/>on getting all images              |
